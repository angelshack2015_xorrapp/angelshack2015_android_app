package angelshack.hackaton.xorrapp;

import android.app.Application;

import angelshack.hackaton.xorrapp.data.network.SwapperService;
import angelshack.hackaton.xorrapp.data.user.UserDTO;
import retrofit.RestAdapter;

/**
 * Created by hussein on 30/05/15.
 */
public class SwapApplication extends Application {
    public static final String ENDPOINT = "http://bc9033ca.ngrok.io/";


    RestAdapter restAdapter;
    private UserDTO data;

    @Override
    public void onCreate() {
        super.onCreate();
        restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://bc9033ca.ngrok.io/")
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        data = new UserDTO();

    }

    public SwapperService getService() {
        return restAdapter.create(SwapperService.class);
    }

    public UserDTO getData() {
        return data;
    }

    public void setData(UserDTO data) {
        this.data = data;
    }
}


