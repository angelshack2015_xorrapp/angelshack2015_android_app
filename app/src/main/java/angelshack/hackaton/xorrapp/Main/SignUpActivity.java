package angelshack.hackaton.xorrapp.Main;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.Image;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import angelshack.hackaton.xorrapp.Main.MainActivity;
import angelshack.hackaton.xorrapp.R;
import angelshack.hackaton.xorrapp.data.SharedPreferencesManager;
import angelshack.hackaton.xorrapp.data.network.AccessToken;
import angelshack.hackaton.xorrapp.data.network.gcm.MyInstanceIDListenerService;
import angelshack.hackaton.xorrapp.data.user.UserDTO;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by hussein on 31/05/15.
 */

public class SignUpActivity extends AppCompatActivity {


    @InjectView(R.id.signup_et_username)
    EditText username;
    @InjectView(R.id.sigup_et_email)
    EditText email;
    @InjectView(R.id.sigup_et_password)
    EditText password;
    @InjectView(R.id.sigup_et_password2)
    EditText password2;

    @InjectView(R.id.login_loading)
    View loading;

    @InjectView(R.id.container)
    View form;

    @InjectView(R.id.iv_avatar)
    ImageView avatar;

    boolean bound = false;
    Bitmap imageBitmap = null;

    SharedPreferencesManager spm;
    MyInstanceIDListenerService registerService;
    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            MyInstanceIDListenerService.MyBinder binder = (MyInstanceIDListenerService.MyBinder) service;
            registerService = binder.getService();
            bound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            bound = false;
        }
    };
    Callback<AccessToken> registerCallback = new Callback<AccessToken>() {
        @Override
        public void success(AccessToken accessToken, Response response) {
            spm.getPreferences().edit().putString(SharedPreferencesManager.TOKEN_LABEL, accessToken.getToken()).apply();
            startMainActivity();
            spm.getPreferences().edit().putBoolean(SharedPreferencesManager.SENT_TOKEN_TO_SERVER, true).apply();
        }

        @Override
        public void failure(RetrofitError error) {
            if (error.getResponse() != null) {

                if (error.getResponse().getStatus() == 403) {
                    hideLoading();
                    username.setError(getString(R.string.wrong_user_pass));
                    password.setError(getString(R.string.wrong_user_pass));
                    showError(getString(R.string.wrong_user_pass));

                } else {
                    username.setError(null);
                    password.setError(null);
                    showError("Unknown error");
                    Log.e("error", "error", error);
                }
            }
        }
    };
    private void hideLoading() {
        loading.setVisibility(View.INVISIBLE);
        form.setVisibility(View.VISIBLE);
    }

    private void showLoading() {
        loading.setVisibility(View.VISIBLE);
        form.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.inject(this);
        spm = new SharedPreferencesManager(getApplicationContext());
        bindService(new Intent(getApplicationContext(),
                MyInstanceIDListenerService.class), connection, BIND_AUTO_CREATE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sign_up, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email)
                .matches();
    }

    @Override
    public void finish() {
        super.finish();
        unbindService(connection);
    }

    private void showError(String err) {
        Snackbar.make(form, err, Snackbar.LENGTH_LONG).show();

    }

    @OnClick(R.id.button_register)
    void createUser() {

        boolean validform = true;

        if (username.getText().toString().equals("")) {
            username.setError("Can't be empty");
            validform = false;
        }
        if (email.getText().toString().equals("")) {
            email.setError("Can't be empty");
            validform = false;
        }
        if (password.getText().toString().equals("")) {
            password.setError("Can't be empty");
            validform = false;
        }
        if (password2.getText().toString().equals("")) {
            password2.setError("Can't be empty");
            validform = false;
        }

        if (!password.getText().toString().equals(password2.getText().toString())) {
            password.setError("The password doesn't match");
            validform = false;
        }

        if (!isEmailValid(email.getText())) {
            email.setError("please verify your email adress");
            validform = false;
        } ;

        if (imageBitmap == null) {
            validform = false;
            Snackbar.make(form, "please take a profile photo", Snackbar.LENGTH_LONG).show();
        }
        if (validform) {
            showLoading();
            UserDTO user = new UserDTO(username.getText().toString(), password.getText().toString(), email.getText().toString());
            registerService.registerUser(user, imageBitmap, registerCallback);
        }
    }

    private void startMainActivity() {
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        finish();
    }
    static final int REQUEST_IMAGE_CAPTURE = 1;

    @OnClick(R.id.iv_avatar) void takePicture() {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
                Bundle extras = data.getExtras();
            imageBitmap = (Bitmap) extras.get("data");
            avatar.setImageBitmap(imageBitmap);
            avatar.setBackgroundColor(Color.TRANSPARENT);
        }
    }
};



