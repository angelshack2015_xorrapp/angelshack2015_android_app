package angelshack.hackaton.xorrapp.Main.Adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import angelshack.hackaton.xorrapp.R;
import angelshack.hackaton.xorrapp.SwapApplication;
import angelshack.hackaton.xorrapp.data.SharedPreferencesManager;
import angelshack.hackaton.xorrapp.model.Ctrl.CtrlSendPhoto;

/**
 * Created by pau on 30/05/2015.
 */
public class RecyclerPhotoMain extends RecyclerView.Adapter<RecyclerPhotoMain.ViewHolder> {
    private final static String TAG = "RecyclerPhotoMain";
    private final String[] usernames;

    private ArrayList<String> dataSet;
    private Context context;
    private CtrlSendPhoto.ICtrlSendPhoto callback;

    public RecyclerPhotoMain(Context context,CtrlSendPhoto.ICtrlSendPhoto callback, ArrayList<String> dataSet,String[] usernames) {
        super();
        this.dataSet = dataSet;
        this.context = context;
        this.usernames = usernames;
        this.callback=callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_photo_view, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {
        final String username=usernames[i];

        Log.e("dataset", dataSet.get(i));
        Picasso.with(context).load(dataSet.get(i)).into(viewHolder.photoView);

//        viewHolder.photoView.setImageDrawable(dataSet.get(i));
        viewHolder.tvUserId.setText(username);
        viewHolder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferencesManager sharedPreferencesManager = new SharedPreferencesManager(context);
                String token = sharedPreferencesManager.getPreferences().getString(SharedPreferencesManager.TOKEN_LABEL, "default");

                new CtrlSendPhoto(context, callback, Camera.CameraInfo.CAMERA_FACING_FRONT)
                        .execute(token, username);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each cacheKey item is just a string in this case
        public ImageView photoView;
        public TextView tvUserId;
        public RelativeLayout layout;

        public ViewHolder(View view) {
            super(view);
            this.photoView = (ImageView) view.findViewById(R.id.iv_photo);
            this.tvUserId=(TextView) view.findViewById(R.id.tv_user_id);
            this.layout=(RelativeLayout) view.findViewById(R.id.layout);
        }
    }
}