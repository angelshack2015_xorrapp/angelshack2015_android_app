package angelshack.hackaton.xorrapp.Main.Adapters;

import android.hardware.Camera;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import java.util.ArrayList;

import angelshack.hackaton.xorrapp.Main.Fragments.FragmentCamera;
import angelshack.hackaton.xorrapp.Main.Fragments.FragmentFrontCamera;
import angelshack.hackaton.xorrapp.Main.Fragments.FragmentRearCamera;

/**
 * Created by pau on 30/05/2015.
 */
public class MainPageAdapter extends FragmentPagerAdapter{
    private final static String TAG="MainPageAdapter";
    private final static int FIRST_ITEM=1;
    public final static int REAR_CHAT_INDEX=2;
    public final static int FRONT_CHAT_INDEX=0;


    public static class BasicOnPageChangeListener implements ViewPager.OnPageChangeListener{
        private final MainPageAdapter adapter;
        private AppCompatActivity activity;


        public BasicOnPageChangeListener(AppCompatActivity activity, MainPageAdapter adapter){
            this.activity=activity;
            this.adapter = adapter;

            if(adapter.getItem(FIRST_ITEM) instanceof FragmentCamera) hideSystemUI();
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            if(adapter.getItem(position) instanceof FragmentCamera){
                hideSystemUI();
            }
            else {
                showSystemUI();
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }

        // This snippet hides the system bars.
        private void hideSystemUI() {
            // Set the IMMERSIVE flag.
            // Set the content to appear under the system bars so that the content
            // doesn't resize when the system bars hide and show.
            activity.getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                            | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                            | View.SYSTEM_UI_FLAG_IMMERSIVE);
        }

        // This snippet shows the system bars. It does this by removing all the flags
// except for the ones that make the content appear under the system bars.
        private void showSystemUI() {
            activity.getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
    }

    private ArrayList<Fragment> fragments = new ArrayList<>();

    public MainPageAdapter(FragmentManager fm) {
        super(fm);
        fragments.add(new FragmentFrontCamera());

        Fragment fragment=new FragmentCamera();
        Bundle args= new Bundle();
        args.putInt(FragmentCamera.ARG_ID_CAMERA, Camera.CameraInfo.CAMERA_FACING_FRONT);
        args.putBoolean(FragmentCamera.ARG_BLOCK_CHANGE_CAMERA,true);
        fragment.setArguments(args);
        fragments.add(fragment);
        fragments.add(new FragmentRearCamera());
    }

    public int getFirstItem(){ return FIRST_ITEM; }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

}
