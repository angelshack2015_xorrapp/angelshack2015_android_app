package angelshack.hackaton.xorrapp.Main;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.test.mock.MockApplication;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import angelshack.hackaton.xorrapp.R;
import angelshack.hackaton.xorrapp.SwapApplication;
import angelshack.hackaton.xorrapp.data.SharedPreferencesManager;
import angelshack.hackaton.xorrapp.data.network.AccessToken;
import angelshack.hackaton.xorrapp.data.network.gcm.MyInstanceIDListenerService;
import angelshack.hackaton.xorrapp.data.user.UserDTO;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class LoginActivity extends AppCompatActivity {

    @InjectView(R.id.login_et_password)
    EditText password;

    @InjectView(R.id.login_et_username)
    EditText username;

    @InjectView(R.id.login_loading)
    View loading;

    @InjectView(R.id.rl_form)
    View form;

    MyInstanceIDListenerService loginService;
    boolean bound = false;

    SharedPreferencesManager spm;
    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            MyInstanceIDListenerService.MyBinder binder = (MyInstanceIDListenerService.MyBinder) service;
            loginService = binder.getService();
            bound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            bound = false;
        }
    };
    Callback<AccessToken> loginCallback = new Callback<AccessToken>() {
        @Override
        public void success(AccessToken accessToken, Response response) {
            spm.getPreferences().edit().putString(SharedPreferencesManager.TOKEN_LABEL, accessToken.getToken()).apply();
            startMainActivity();
            spm.getPreferences().edit().putBoolean(SharedPreferencesManager.SENT_TOKEN_TO_SERVER, true).apply();
            ((SwapApplication) getApplication()).getService().getUserData(accessToken.getToken(), new Callback<UserDTO>() {
                @Override
                public void success(UserDTO userDTO, Response response) {
                    ((SwapApplication) getApplication()).setData(userDTO);
                    Log.e("", "GOOD!");

                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e("", "Fail!");

                }
            });
        }

        @Override
        public void failure(RetrofitError error) {
            hideLoading();
            if (error.getResponse() != null) {
                if (error.getResponse().getStatus() == 403) {
                    username.setError(getString(R.string.wrong_user_pass));
                    password.setError(getString(R.string.wrong_user_pass));
                    showError(getString(R.string.wrong_user_pass));


                } else if (error.getResponse().getStatus() == 409) {
                    username.setError(getString(R.string.user_exists));


                } else {
                    username.setError(null);
                    password.setError(null);
                    showError("Unknown error");
                    Log.e("error", "error", error);
                }
            } else {

                showError("Unknown error");
            }
        }


    };

    private void startMainActivity() {
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        finish();
    }

    private void showError(String err) {
        Snackbar.make(form, err, Snackbar.LENGTH_LONG).show();

    }

    private void hideLoading() {
        loading.setVisibility(View.INVISIBLE);
        form.setVisibility(View.VISIBLE);
    }

    private void showLoading() {
        loading.setVisibility(View.VISIBLE);
        form.setVisibility(View.INVISIBLE);
    }

    @OnClick(R.id.button_login)
    public void login() {
        Log.i("info", "info");
        showLoading();
        try {

            UserDTO user = new UserDTO(username.getText().toString(), password.getText().toString());
            loginService.login(user, loginCallback);

        } catch (Exception e) {
            hideLoading();
            showError(getString(R.string.generic_error));
            Log.e("Error", "error", e);
        }
    }

    @OnClick(R.id.button_register)
    public void signup() {
        startActivity(new Intent(getApplicationContext(), SignUpActivity.class));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Crouton.cancelAllCroutons();
    }

    @Override
    public void finish() {
        super.finish();
        try {
            unbindService(connection);
        } catch (Exception e) {

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        spm = new SharedPreferencesManager(getApplicationContext());
        if (spm.getPreferences().getString(
                SharedPreferencesManager.TOKEN_LABEL, "default").equals("default")) {

            setContentView(R.layout.activity_login);
            ButterKnife.inject(this);
            bindService(new Intent(getApplicationContext(),
                    MyInstanceIDListenerService.class), connection, BIND_AUTO_CREATE);
        } else {
            startMainActivity();
        }

        ;


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
