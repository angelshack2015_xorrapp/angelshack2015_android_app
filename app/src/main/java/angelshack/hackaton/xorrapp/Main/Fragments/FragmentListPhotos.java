package angelshack.hackaton.xorrapp.Main.Fragments;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.test.mock.MockApplication;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import angelshack.hackaton.xorrapp.Main.Adapters.RecyclerPhotoMain;
import angelshack.hackaton.xorrapp.R;
import angelshack.hackaton.xorrapp.SwapApplication;
import angelshack.hackaton.xorrapp.data.user.ConservationDTO;
import angelshack.hackaton.xorrapp.model.Ctrl.CtrlSendPhoto;

/**
 * Created by pau on 30/05/2015.
 */
public class FragmentListPhotos extends Fragment implements CtrlSendPhoto.ICtrlSendPhoto {
    private final static String TAG="FragmentListPhotos";

    private RecyclerView recyclerView;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list_photos, container, false);
        recyclerView=(RecyclerView) rootView.findViewById(R.id.recycler);

        GridLayoutManager mGridManager = new GridLayoutManager(getActivity(),
                getResources().getInteger(R.integer.num_col_recycler_photos)
                ,GridLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(mGridManager);

        test();

        return rootView;
    }

    public void setPhotos(ArrayList<String> photos){
        String[] usernames = getResources().getStringArray(R.array.list_usernames);
        recyclerView.setAdapter(new RecyclerPhotoMain(getActivity(),this,photos,usernames));

    }

    public void test(){

        ArrayList<String> photos = new ArrayList<>();
        for (ConservationDTO conservationDTO : ((SwapApplication) getActivity().getApplication()).getData().getConversations()) {
            photos.add(conservationDTO.getImageUrl().toString());
            Log.i(TAG, conservationDTO.getImageUrl().toString());
        }
        setPhotos(photos);
    }

    @Override
    public void onSuccess() {

    }

    @Override
    public void onError(String error, int errorCode) {

    }
}
