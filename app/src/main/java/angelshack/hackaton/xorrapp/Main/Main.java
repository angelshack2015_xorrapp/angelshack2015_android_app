package angelshack.hackaton.xorrapp.Main;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.google.gson.Gson;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Random;

import angelshack.hackaton.xorrapp.R;
import angelshack.hackaton.xorrapp.data.user.UserDTO;

/**
 * Created by hussein on 30/05/15.
 */
public class Main {

    private static class Users {

    }
    public static void main (String [ ] args) {
        ArrayList <UserDTO> users = new ArrayList<>();

        PrintWriter writer = null;
        try {
            writer = new PrintWriter("the-file-name.json", "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        Random random = new Random();
        for (int i = 0; i < 50; i++) {
            ArrayList<String> friends = new ArrayList<>();
            for (int j = 0; j < 4; j++) {
                friends.add("User" + (Math.abs(random.nextInt()%50)));

            }
            users.add(new UserDTO("User" + i, i+"@agar.com", i+"", friends, "gcm"));
        }

        writer.println(new Gson().toJson(users));
        writer.close();
    }

}
