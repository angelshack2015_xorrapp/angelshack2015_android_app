package angelshack.hackaton.xorrapp.Main;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;

import angelshack.hackaton.xorrapp.Main.Adapters.MainPageAdapter;
import angelshack.hackaton.xorrapp.Main.Fragments.FragmentCamera;
import angelshack.hackaton.xorrapp.R;
import angelshack.hackaton.xorrapp.data.SharedPreferencesManager;


public class MainActivity extends AppCompatActivity {
    private ViewPager viewPager;
    private MainPageAdapter pagerAdapter;
    private SharedPreferencesManager spm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager=(ViewPager) findViewById(R.id.pager);
        pagerAdapter= new MainPageAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        viewPager.setCurrentItem(pagerAdapter.getFirstItem());

        viewPager.addOnPageChangeListener(new MainPageAdapter.BasicOnPageChangeListener(this, pagerAdapter));
        spm = new SharedPreferencesManager(getApplicationContext());

    }

    public void showPage(int position){
        viewPager.setCurrentItem(position);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            logout();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        int availability = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(getApplicationContext());
        if (availability != ConnectionResult.SUCCESS) {
            GoogleApiAvailability.getInstance().getErrorDialog(this, availability, 1);
        }
    }

    private void logout() {
        Log.e("LOG", "Logout");
        spm.getPreferences().edit().remove(SharedPreferencesManager.TOKEN_LABEL).apply();
        startActivity(new Intent(getApplicationContext(), LoginActivity.class));

    }
}
