package angelshack.hackaton.xorrapp.data.user;

import com.google.gson.Gson;
import com.squareup.okhttp.internal.Util;

import java.util.ArrayList;

/**
 * Created by hussein on 30/05/15.
 */
public class UserDTO {

    String username;
    String email;
    String password;
    private ArrayList<String> friends;
    private ArrayList<ConservationDTO> conversations;

    String gcm;

    public UserDTO() {
        friends = new ArrayList<>();
        conversations = new ArrayList<>();
    }

    public String getGcm() {
        return gcm;
    }

    public void setGcm(String gcm) {
        this.gcm = gcm;
    }

    public UserDTO(String username, String password) {
        this.username = username;
        this.password = Util.md5Hex(password);
    }

    public UserDTO(String username, String password, String email) {
        this.username = username;
        this.email = email;
        this.password = Util.md5Hex(password);
    }

    public UserDTO(String username, String email, String password, ArrayList <String> friends, String gcm) {
        this.username = username;
        this.email = email;
        this.password = Util.md5Hex(password);
        this.friends = friends;
        this.gcm = gcm;
    }

    public UserDTO(String username, String email, String password, ArrayList <String> friends) {
        this.username = username;
        this.email = email;
        this.password = Util.md5Hex(password);
        this.friends = friends;
    }

    public String toJson() {
        return new Gson().toJson(this);
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public ArrayList<ConservationDTO> getConversations() {
        return conversations;
    }
}
