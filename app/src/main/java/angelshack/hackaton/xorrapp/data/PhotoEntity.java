package angelshack.hackaton.xorrapp.data;

import com.squareup.okhttp.HttpUrl;

import java.net.URL;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by hussein on 30/05/15.
 */
public class PhotoEntity extends RealmObject implements NetworkData {

    @PrimaryKey
    private String id;

    @Ignore
    private String Url;

    public void setUrl(String url) {
        Url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public HttpUrl getUrl(String id) {
        StringBuilder builder = new StringBuilder();
        builder.append(ENDPOINT);
        builder.append(id);
        return HttpUrl.parse(builder.toString());
    }


}
