package angelshack.hackaton.xorrapp.data.network;

/**
 * Created by hussein on 31/05/15.
 */
public class NetworkResult {
    String error = "";

    boolean hasError() {
        return !error.equals("");
    }
}
