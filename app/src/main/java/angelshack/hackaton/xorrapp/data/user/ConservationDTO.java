package angelshack.hackaton.xorrapp.data.user;

import com.google.gson.annotations.SerializedName;
import com.squareup.okhttp.HttpUrl;

import angelshack.hackaton.xorrapp.SwapApplication;

/**
 * Created by hussein on 31/05/15.
 */
public class ConservationDTO {
    private static final String IMAGE_FORMAT = ".jpg";
    String sender;
    String receiver;
    @SerializedName("isRead")
    boolean read;
    String image;


    public HttpUrl getImageUrl() {
        return HttpUrl.parse(SwapApplication.ENDPOINT +"image/" + image);
    }
}
