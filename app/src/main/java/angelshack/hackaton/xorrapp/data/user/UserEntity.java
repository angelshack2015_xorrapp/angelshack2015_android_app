package angelshack.hackaton.xorrapp.data.user;

import angelshack.hackaton.xorrapp.data.ConversationEntity;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by hussein on 30/05/15.
 */
public class UserEntity extends RealmObject {
    @PrimaryKey
    private String username;
    private RealmList<ConversationEntity> frontConversations;
    private RealmList<ConversationEntity> rearConversations;
    private RealmList<UserEntity> friends;

    public RealmList<UserEntity> getFriends() {
        return friends;
    }

    public void setFriends(RealmList<UserEntity> friends) {
        this.friends = friends;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public RealmList<ConversationEntity> getFrontConversations() {
        return frontConversations;
    }

    public void setFrontConversations(RealmList<ConversationEntity> frontConversations) {
        this.frontConversations = frontConversations;
    }

    public RealmList<ConversationEntity> getRearConversations() {
        return rearConversations;
    }

    public void setRearConversations(RealmList<ConversationEntity> rearConversations) {
        this.rearConversations = rearConversations;
    }
}
