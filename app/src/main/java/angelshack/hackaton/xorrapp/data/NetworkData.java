package angelshack.hackaton.xorrapp.data;


import com.squareup.okhttp.HttpUrl;

/**
 * Created by hussein on 30/05/15.
 */
public interface NetworkData {
    public static final String ENDPOINT = "http://192.168.20.99";

    HttpUrl getUrl(String id);
}
