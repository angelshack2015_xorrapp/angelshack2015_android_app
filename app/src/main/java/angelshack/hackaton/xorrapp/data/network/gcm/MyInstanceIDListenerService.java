package angelshack.hackaton.xorrapp.data.network.gcm;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import angelshack.hackaton.xorrapp.R;
import angelshack.hackaton.xorrapp.SwapApplication;
import angelshack.hackaton.xorrapp.data.SharedPreferencesManager;
import angelshack.hackaton.xorrapp.data.network.AccessToken;
import angelshack.hackaton.xorrapp.data.user.UserDTO;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

/**
 * Created by hussein on 31/05/15.
 */
public class MyInstanceIDListenerService extends IntentService {

    private final IBinder binder = new MyBinder();

    public MyInstanceIDListenerService(String name) {
        super(name);
    }

    public MyInstanceIDListenerService() {
        super("LoginService");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    protected void onHandleIntent(Intent intent) {

    }

    public void login(final UserDTO user, final Callback<AccessToken> callback) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                String token = null;
                SharedPreferencesManager sharedPreferencesManager = new SharedPreferencesManager(getApplicationContext());

                try {
                    InstanceID instanceID = InstanceID.getInstance(getApplicationContext());

                    token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                            GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                ((SwapApplication) getApplication()).getService().login(user.getUsername(),
                        user.getPassword(), token, callback);
                Log.e("TAG", "GCM Registration Token: " + token);
                sharedPreferencesManager.getPreferences().edit().
                        putBoolean(SharedPreferencesManager.SENT_TOKEN_TO_SERVER, true).apply();

            }
        }).start();


    }

    public void registerUser(final UserDTO user, final Bitmap photo, final Callback<AccessToken> registerCallback) {

        new Thread(new Runnable() {
            @Override
            public void run() {


                File imageFileFolder = new File(getCacheDir(), "Avatar");
                if (!imageFileFolder.exists()) {
                    imageFileFolder.mkdir();
                }

                FileOutputStream out = null;

                File imageFileName = new File(imageFileFolder, "avatar-" + System.currentTimeMillis() + ".jpg");
                try {
                    out = new FileOutputStream(imageFileName);
                    photo.compress(Bitmap.CompressFormat.JPEG, 100, out);
                    out.flush();
                } catch (IOException e) {
                    Log.e("", "Failed to convert image to JPEG", e);
                } finally {
                    try {
                        if (out != null) {
                            out.close();
                        }
                    } catch (IOException e) {
                        Log.e("", "Failed to close output stream", e);
                    }
                }


                TypedFile image = new TypedFile("image/jpeg", imageFileName);
                String token = null;
                SharedPreferencesManager sharedPreferencesManager = new SharedPreferencesManager(getApplicationContext());

                try {
                    InstanceID instanceID = InstanceID.getInstance(getApplicationContext());

                    token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                            GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                ((SwapApplication) getApplication()).getService().register(
                        user.getUsername(),
                        user.getPassword(),
                        user.getEmail(),
                        token,
                        registerCallback);
                Log.e("TAG", "GCM Registration Token: " + token);

                ((SwapApplication) getApplication()).getService().uploadAvatar(token, image, new Callback<String>() {
                    @Override
                    public void success(String s, Response response) {
                        Log.e("·", "Uploaded");
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.e(".", ",", error);
                    }
                });
                sharedPreferencesManager.getPreferences().edit().
                        putBoolean(SharedPreferencesManager.SENT_TOKEN_TO_SERVER, true).apply();

            }
        }).start();
    }

    public class MyBinder extends Binder {
        public MyInstanceIDListenerService getService() {
            return MyInstanceIDListenerService.this;
        }
    }
}
