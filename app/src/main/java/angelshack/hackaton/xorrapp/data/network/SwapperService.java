package angelshack.hackaton.xorrapp.data.network;


import angelshack.hackaton.xorrapp.data.user.UserDTO;
import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.mime.TypedFile;

/**
 * Created by hussein on 31/05/15.
 */
public interface SwapperService {
    @POST("/auth/login/")
    @FormUrlEncoded
    void login(@Field("username") String username,
               @Field("password") String password,
               @Field("gcm") String gcm,
               Callback<AccessToken> callback);

    @POST("/auth/register/")
    @FormUrlEncoded
    void register(@Field("username") String username,
                  @Field("password") String password,
                  @Field("email") String email,
                  @Field("gcm") String gcm,
                  Callback<AccessToken> callback);

    @POST("/avatar")
    @Multipart
    void uploadAvatar(@Query("access_token") String token, @Part("avatar") TypedFile image, Callback<String> callback);


    @POST("/image")
    @Multipart
    void uploadImage(@Query("access_token") String token,
                     @Query("receiver") String receiver,
                     @Part("avatar") TypedFile image,
                     Callback<String> callback);


    @GET("/user")
    void getUserData(@Query("access_token") String accessToken, Callback<UserDTO> callback);

}

