package angelshack.hackaton.xorrapp.data;

import angelshack.hackaton.xorrapp.data.user.UserEntity;
import io.realm.RealmObject;

/**
 * Created by hussein on 30/05/15.
 */
public class ConversationEntity extends RealmObject {
    private PhotoEntity lastReceived;
    private PhotoEntity lastSent;
    private UserEntity sender;
    private UserEntity receiver;

    public PhotoEntity getLastReceived() {
        return lastReceived;
    }

    public void setLastReceived(PhotoEntity lastReceived) {
        this.lastReceived = lastReceived;
    }

    public PhotoEntity getLastSent() {
        return lastSent;
    }

    public void setLastSent(PhotoEntity lastSent) {
        this.lastSent = lastSent;
    }

    public UserEntity getSender() {
        return sender;
    }

    public void setSender(UserEntity sender) {
        this.sender = sender;
    }

    public UserEntity getReceiver() {
        return receiver;
    }

    public void setReceiver(UserEntity receiver) {
        this.receiver = receiver;
    }
}
