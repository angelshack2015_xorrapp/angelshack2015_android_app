package angelshack.hackaton.xorrapp.data;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by hussein on 31/05/15.
 */
public class SharedPreferencesManager {
    private static final String PREFS_NAME = "App";
    public static final String TOKEN_LABEL = "accessToken";
    public static final String SENT_TOKEN_TO_SERVER = "sentToken";

    private final Context context;
    SharedPreferences preferences;

    public SharedPreferencesManager(Context context) {
        this.context = context;
        preferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    public SharedPreferences getPreferences() {
        return preferences;
    }
}
