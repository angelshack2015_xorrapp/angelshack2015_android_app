package angelshack.hackaton.xorrapp.model;

/**
 * Created by hussein on 30/05/15.
 */
public class Conversation {
    Photo lastReceived;
    Photo lastSent;
    User sender;
    User receiver;
    boolean readed;

    public Conversation(User sender, User receiver) {
        this.sender = sender;
        this.receiver = receiver;
    }

    public boolean isReaded() {
        return readed;
    }
}
