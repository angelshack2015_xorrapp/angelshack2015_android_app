package angelshack.hackaton.xorrapp.model;

import java.util.ArrayList;

/**
 * Created by hussein on 30/05/15.
 */
public class Conversations {
    private ArrayList<Conversation> frontConversations;
    private ArrayList <Conversation> rearConversations;

    public Conversations() {
        frontConversations = new ArrayList<>();
        rearConversations = new ArrayList<>();
    }

    public Conversations getReadedConversations() {
        Conversations newConservations = new Conversations();
        for (Conversation frontConversation : frontConversations) {
            if (frontConversation.isReaded()) newConservations.addFrontConversation(frontConversation);
        }

        for (Conversation rearConversation : rearConversations) {
            if (rearConversation.isReaded()) newConservations.addRearConversation(rearConversation);
        }

        return newConservations;
    }

    public void addFrontConversation(Conversation frontConversation) {
        frontConversations.add(frontConversation);
    }

    public void addRearConversation(Conversation frontConversation) {
        frontConversations.add(frontConversation);
    }
}
