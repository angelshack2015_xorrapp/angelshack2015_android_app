package angelshack.hackaton.xorrapp.model.Ctrl;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import angelshack.hackaton.xorrapp.SwapApplication;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

/**
 * Created by pau on 31/05/2015.
 */
public class CtrlSendPhoto {
    private final ICtrlSendPhoto iCtrl;
    private final int idCamera;
    private final Context context;

    public interface ICtrlSendPhoto{
        public void onSuccess();
        public void onError(String error, int errorCode);
    }



    public CtrlSendPhoto(Context context,ICtrlSendPhoto iCtrl,int idCamera){
        this.context = context;
        this.iCtrl = iCtrl;
        this.idCamera = idCamera;
    }

    public void execute(final String token, final String receiverUsername){

    }
}
