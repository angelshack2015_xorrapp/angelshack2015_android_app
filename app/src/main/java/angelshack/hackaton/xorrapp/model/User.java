package angelshack.hackaton.xorrapp.model;

import java.util.ArrayList;

/**
 * Created by hussein on 30/05/15.
 */
public class User {
    private final Conversations conversations;
    private String username;
    private ArrayList <User> friends;
    private Photo avatar;

    public User(String username) {
        this.username = username;
        conversations = new Conversations();
    }

    public Conversations gerReadedConversations() {
       return conversations.getReadedConversations();
    }
}
